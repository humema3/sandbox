#!/bin/bash
## -S /bin/bash
## -N oncor.ccle.data.post
## -t 1
## -t 1-42
## -m n
## -j y
## -l h_rt=345599

DATA_DIR=/da/onc/ONCOR/data/oncor-solr/1.8.0
#CSV_FILE=`ls $DATA_DIR/*.data.csv | head -n $SGE_TASK_ID | tail -n 1`
for CSV_FILE in $DATA_DIR/*.data.csv; do
    #if [ "`basename $CSV_FILE`" == "LOF.data.csv" ] || [[ "`basename $CSV_FILE`" =~ ^[M-Zm-z].+ ]]; then
        CUT_CSV_FILE=$(dirname $(dirname `readlink -f $0`))/data/`basename $CSV_FILE`
        if [ ! -f $CUT_CSV_FILE ]; then
            echo "Extracting columns from $CSV_FILE to $CSV_FILE: `date +\"%F %T\"`" >&2
            HEADER_LINE=`sed '1q;d' $CSV_FILE`
            mkdir -p `dirname $CUT_CSV_FILE`
            FEATURE_NAME_COL_NUM=`echo $HEADER_LINE | tr " " "\n" | grep -n "featureName" | cut -d':' -f1`
            FEATURE_OBJECT_ID_COL_NUM=`echo $HEADER_LINE | tr " " "\n" | grep -n "featureObjectId" | cut -d':' -f1`
            FEATURE_PROPERTY_NAME_COL_NUM=`echo $HEADER_LINE | tr " " "\n" | grep -n "featurePropertyName" | cut -d':' -f1`
            FEATURE_VALUE_COL_NUM=`echo $HEADER_LINE | tr " " "\n" | grep -n "^featureValue$" | cut -d':' -f1`
            SMF_UID_COL_NUM=`echo $HEADER_LINE | tr " " "\n" | grep -n "smfUid" | cut -d':' -f1`
            cut -f${FEATURE_NAME_COL_NUM},${FEATURE_OBJECT_ID_COL_NUM},${FEATURE_PROPERTY_NAME_COL_NUM},${FEATURE_VALUE_COL_NUM},${SMF_UID_COL_NUM} $CSV_FILE > $CUT_CSV_FILE
        #fi

        echo "Posting to SOLR: `date +\"%F %T\"`" >&2
        #echo $CUT_CSV_FILE
        /opt/solr/bin/post -c onc.data.ccle -params "separator=%09" -type text/csv $CUT_CSV_FILE
    fi
done
