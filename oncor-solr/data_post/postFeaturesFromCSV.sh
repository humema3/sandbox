#!/bin/bash

DATA_DIR=/da/onc/ONCOR/data/oncor-solr/1.8.0
for CSV_FILE in $DATA_DIR/*.features.csv; do
    CMD="/opt/solr/bin/post -c onc.features.ccle -params "separator=%09" -type text/csv $CSV_FILE"
    echo $CMD
    $CMD
done
