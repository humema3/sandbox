#!/bin/sh
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureName, type:text_general, multiValued:false}}' http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureObjectId, type:string, multiValued:false}}' http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featurePropertyName, type:text_general, multiValued:false}}' http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureValue, type:tdouble, multiValued:false}}' http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:smfUid, type:string, multiValued:false}}' http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema
