#!/bin/sh
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureName, type:text_general, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureObjectId, type:string, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureObjectName, type:text_general, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featureObjectType, type:string, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featurePropertyName, type:text_general, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
curl -X POST -H 'Content-type:application/json' --data-binary '{"add-field":{name:featurePropertyType, type:string, multiValued:false}}' http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema
