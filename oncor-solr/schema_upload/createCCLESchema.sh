#!/bin/sh

OUTPUT_DIR="./output"

#CCLE_BSR_SCHEMA="http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.bsr.ccle_shard1_replica1/schema"
#BSR_FIELDS=(SMF_UID PRIMARY_SITE CLEANNAME CLEAN_ALIAS_NAMES BIOSAMPLE_NAME HISTOLOGY PATHOLOGY PATHOLOGIST_ANNOTATION CURATION_STATUS SAMPLE_NAME CELLS PRIMARY_SITE_SUBTYPE1 PRIMARY_SITE_SUBTYPE2 PRIMARY_SITE_SUBTYPE3 HIST_SUBTYPE1 HIST_SUBTYPE2 HIST_SUBTYPE3 SPECIES GENDER RACE SITE_OF_FINDING PATIENT_DIAGNOSIS GEO_LOC AGE_YRS LIFE_STAGE CLE_FLAG CLIP_FLAG)

#CCLE_DATA_SCHEMA="http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.data.ccle_shard4_replica1/schema"
#DATA_FIELDS=(featureName featureObjectId featurePropertyName featureValue smfUid)

#CCLE_FEATURES_SCHEMA="http://nrusca-slt0219.nibr.novartis.net:8983/solr/onc.features.ccle_shard1_replica1/schema"
#FEATURES_FIELDS=(featureName featureObjectId featureObjectName featureObjectType featurePropertyName featurePropertyType)

CCLE_MUTATION_PROFILE_SCHEMA="http://nrusca-slt0220.nibr.novartis.net:8983/solr/onc.mutationProfile.ccle_shard4_replica1/schema"
MUTATION_PROFILE_FIELDS=( GENE_SYMBOL ENTREZ_GENE_ID CONCEPT_SMF_UID CONSENSUS_CALL CONSENSUS_ALLELE_BALANCE CONSENSUS_DEPTH MUT_LEVEL VARIANT_CLASS FUNCTIONAL_EFFECT AMINO_ACID_CHANGE CN_LOG2RATIO_MEAN GOF_CALL GOF_LEVEL CLEAN_NAME PRIMARY_SITE HISTOLOGY LOF_CALL LOF_LEVEL )
MUTATION_PROFILE_FIELD_TYPES=( string tlong string string tdouble string tlong string string string tdouble string tlong string string string string tlong )


mkdir -p ${OUTPUT_DIR}
#rm ${OUTPUT_DIR}/ccle_*.sh
#echo "#!/bin/sh" > ${OUTPUT_DIR}/ccle_bsr.sh
#echo "#!/bin/sh" > ${OUTPUT_DIR}/ccle_data.sh
#echo "#!/bin/sh" > ${OUTPUT_DIR}/ccle_features.sh
echo "#!/bin/sh" > ${OUTPUT_DIR}/ccle_mutation_profile.sh

#for field in ${BSR_FIELDS[@]}; do
#    echo "curl -X POST -H 'Content-type:application/json' --data-binary '{\"add-field\":{name:"$field",  type:string, multiValued:false}}'" ${PTX_BSR_SCHEMA} >>${OUTPUT_DIR}/ccle_bsr.sh
#done

#for field in ${DATA_FIELDS[@]}; do
#    if [ $field == "featureValue" ]; then
#        dataType='tdouble'
#    elif [[ $field =~ .+Name ]]; then
#        dataType='text_general'
#    else
#        dataType='string'
#    fi
#    echo "curl -X POST -H 'Content-type:application/json' --data-binary '{\"add-field\":{name:"$field", type:$dataType, multiValued:false}}'" ${CCLE_DATA_SCHEMA} >> ${OUTPUT_DIR}/ccle_data.sh
#done

#for field in ${FEATURES_FIELDS[@]}; do
#    if [[ "$field" =~ .+Name ]]; then
#        dataType='text_general'
#    else
#        dataType='string'
#    fi
#    echo "curl -X POST -H 'Content-type:application/json' --data-binary '{\"add-field\":{name:"$field", type:$dataType, multiValued:false}}'" ${CCLE_FEATURES_SCHEMA} >>${OUTPUT_DIR}/ccle_features.sh
#done

for i in `seq 1 ${#MUTATION_PROFILE_FIELDS[@]}`; do
    field=${MUTATION_PROFILE_FIELDS[$((i-1))]}
    dataType=${MUTATION_PROFILE_FIELD_TYPES[$((i-1))]}
    echo "curl -X POST -H 'Content-type:application/json' --data-binary '{\"add-field\":{name:"$field", type:$dataType, multiValued:false}}'" ${CCLE_MUTATION_PROFILE_SCHEMA} >> ${OUTPUT_DIR}/ccle_mutation_profile.sh
done

exit 0

 
