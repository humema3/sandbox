import jaydebeapi
import os
import sys
from argparse import ArgumentParser
from datetime import datetime

def updateSmfIds(labeledSeqs, cursor):
    # These two CRISPR guides from the cBGPD library share sequences on opposite strands with sequences from EPICPool2.
    # This is a hack based on the assumption that we are only updating the EPICPool* SMF ids.
    # We do not want to change the distinct reverse strand sequences for cBGPD.
    # The two EPICPool2 SMF ids are already properly set so do not need to be updated manually.
    # If we actually want to update cBGPD SMF ids for cBGPD then this will have to change.
    # A permanent solution would be quite complicated and would have to utilize the value of libs in the dictionary, which are currently ignored.
    dontChangeTheseSequences = ('CGTCAGGAAGGACGACCGCA', 'GCGAACTGTGACTGTCTAAA')
    stmt = "UPDATE CRISPR_GUIDE cg SET cg.SMF_ID = :1 WHERE cg.SEQ = :2"
    params = []
    for sequence, (smfId, libs) in labeledSeqs.items():
        if sequence not in dontChangeTheseSequences:
            params.append((smfId, sequence))
    cursor.executemany(stmt, params)


def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def main(argv):
    parser = ArgumentParser()
    parser.add_argument('-f', '--lib_files', dest='lib_files', nargs="+")
    parser.add_argument('-pools', '--pool_names', dest='pool_names', nargs="*")
    parser.add_argument('-pw', '--password', dest='password', required=True, type=str)
    parser.add_argument('-u', '--user', dest='user', required=False, type=str, default='wbxtco')
    parser.add_argument('-url', '--url', dest='url', required=False, type=str, default="jdbc:oracle:thin:@ldap://tns.na.novartis.net:389/WBXTCO_USCA_DEV,cn=oraclecontext,dc=research,dc=pharma")
    parser.add_argument('-o', '--oracle_driver_jar_path', dest='oracle_driver_jar_path', required=False, type=str, default='/home/humema3/ojdbc7.jar')
    args = parser.parse_args(argv)

    libFiles = args.lib_files
    poolNames = args.pool_names
    password = args.password
    url = args.url
    user = args.user
    oracleDriverJarPath = args.oracle_driver_jar_path

    if poolNames is None or len(poolNames) == 0:
        poolNames = [os.path.splitext(os.path.basename(f))[0] for f in libFiles]
    assert len(poolNames) == len(libFiles), "Numbers of library files and pool names do not match"

    conn = jaydebeapi.connect('oracle.jdbc.driver.OracleDriver', [url, user, password], oracleDriverJarPath)
    curs = conn.cursor()

    sys.stderr.write("Building sequence dictionary\n")
    labeledSeqs = {}
    for i, libFile in enumerate(libFiles):
        f = open(libFile, 'r')
        while True:
            smf = f.readline()
            seq = f.readline()
            if not seq:
                break
            smf = smf.strip()[1:]
            seq = seq.strip()
            assert seq not in labeledSeqs or labeledSeqs[seq][0] == smf, "Sequence maps to more than one SMF id in files: " + labeledSeqs[seq] + ", " + smf
            if seq not in labeledSeqs:
                labeledSeqs[seq] = (smf, [])
            labeledSeqs[seq][1].append(poolNames[i])
        f.close()

    sys.stderr.write("Updating database\n")
    updateSmfIds(labeledSeqs, curs)

    curs.close()
    conn.close()

if __name__ == "__main__":
    main(sys.argv[1:])